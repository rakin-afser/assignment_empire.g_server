const express = require('express');
var app = express();
const _ = require('lodash');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken')
var cors = require('cors');
var passport = require('passport');
var passportJWT = require('passport-jwt');

const JWTstrategy = passportJWT.Strategy ;
const extractJWT = passportJWT.ExtractJwt ;

var jwtOptions = {};
jwtOptions.jwtFromRequest = extractJWT.fromAuthHeader();
jwtOptions.secretOrKey = 'rakin';

var authentication = require('./authentication');
var users=[];
// Saving initialData ...
authentication.initialData();
setTimeout(function(){
  authentication.users().then((res)=> {
    users = res;
    console.log('Users Initialized');
    //console.log('User -> ',users);

  }).catch((err)=> {
    console.log('Error ->',err);
  })
},2000)

var strategy = new JWTstrategy(jwtOptions, function(jwt_payload, next) {
  console.log( ' Payload received - ', jwt_payload);
  //extract object from token and then find id/userid from user array
  var user = users[_.findIndex(users,{id:jwt_payload.id})]

  if(user) {
    next(null, user)
  } else {
    next(null, false)
  }
})
app.use(cors(({credentials: true, origin: true})))

passport.use(strategy);
app.use(passport.initialize());
app.use(bodyParser.urlencoded({
  extended : true
}))
app.use(bodyParser.json())

app.get('/', (req,res) => {
  res.json( {msg : 'Triggered '})
})

app.post('/login',function(req,res) {
  if(req.body.userId && req.body.password){
    var userId = req.body.userId;
    var password = req.body.password;
  }

  var user = users[_.findIndex(users, {userId: userId})];

  if( ! user ){
    res.status(401).json({message:"no such user found"});
  }

  if(user.password === req.body.password) {
    // from now on we'll identify the user by the id and the id is the only personalized value that goes into our token
    var payload = {id: user.id,
    user : user};
    var token = jwt.sign(payload, jwtOptions.secretOrKey);
    res.json({message: "ok", token: token, user : user});
    console.log(user,'-------');
    //res.send(user)
  } else {
    res.status(401).json({message:"passwords did not match"});
  }

})

app.get("/secret", passport.authenticate('jwt', { session: false }), function(req, res){
  res.json("Success! You can not see this without a token");
});




app.listen(3000, () => {
  console.log(' Express started at 3000');
})
