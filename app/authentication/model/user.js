var _path = require('../../path');
var mongoose = require('mongoose');
mongoose.connect(_path.db_path)
var Schema = mongoose.Schema;

var userSchema = new Schema ({
  name : String ,
  userId : { type : String,  required : true, unique : true},
  password : { type : String,  required : true },

})

exports.User = mongoose.model('User',userSchema);
